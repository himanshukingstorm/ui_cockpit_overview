from flask import Flask, request, render_template
import os
import random
import base64
import subprocess
app = Flask(__name__, static_url_path='/static')

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/submit_form', methods=['POST'])
def submit_form():
    # Check if a file was uploaded
    if 'jsonFile' not in request.files:
        return 'No file part'

    json_file = request.files['jsonFile']

    # Check if the file has a filename
    if json_file.filename == '':
        return render_template('./file_submit.html')

    # Check if the file is a JSON file
    if not json_file.filename.endswith('.json'):
        return render_template('./file_submit.html')
    


    # Specify the directory where you want to save the JSON file
    save_directory = './'

    # Save the JSON file with its original filename
    json_file.save(f"{save_directory}/{json_file.filename}")



    User_name = request.form.get('User_name')
    User_Id = str(int(random.random()))

    # Azure Key Vault and Secrets Configuration
    key_vault_name = User_name + User_Id
    resource_group_name = "prashant-rg"
    location = "westus2"
    secrets_file_path = json_file.filename
        

        # Create Azure Key Vault if it doesn't exist
    create_kv_command = f"az keyvault create --name {key_vault_name} --resource-group {resource_group_name} --location {location}"
    try:
            subprocess.check_output(create_kv_command, shell=True)
            print(f"Azure Key Vault '{key_vault_name}' created successfully in Resource Group '{resource_group_name}'.")
    except subprocess.CalledProcessError:
            print(f"Error: Failed to create Azure Key Vault.")
            exit(1)

        

        # Authenticate to Azure
    try:
            # Use Azure CLI to get the access token
            access_token = subprocess.check_output(["az", "account", "get-access-token", "--query", "accessToken", "-o", "tsv"]).decode("utf-8").strip()
    except subprocess.CalledProcessError:
            print("Error: Failed to obtain Azure access token. Make sure you are logged into Azure CLI.")
            exit(1)

        

        # Read the entire content of the JSON file
    with open(secrets_file_path, 'r') as json_file:
            secrets_content = json_file.read()

        

        # Store the entire JSON content as a secret
    secret_name = "your-secret-name"
    encoded_value = base64.b64encode(secrets_content.encode("utf-8")).decode("utf-8")     
    command = f"az keyvault secret set --vault-name {key_vault_name} --name {secret_name} --value {encoded_value} --output none --query 'value'"
          # Replace with your desired secret name
    # command = f"az keyvault secret set --vault-name {key_vault_name} --name {secret_name} --value '{secrets_content}' --output none --query 'value'"
    try:
            # Use Azure CLI to set the secret in the Key Vault
            subprocess.check_call(["bash", "-c", f'AZURE_ACCESS_TOKEN="{access_token}" {command}'])
            print(f"Secret '{secret_name}' has been stored in Azure Key Vault.")
    except subprocess.CalledProcessError as e:
            print(f"Error: Failed to store secret '{secret_name}' in Azure Key Vault.")
            print(e)

        

    print("Secret has been stored in Azure Key Vault.")
    os.remove(secrets_file_path)     
    

   

    return render_template('./file_submit.html')

if __name__ == '__main__':
    app.run(debug=True)
