from flask import Flask, request, render_template
import os
import subprocess
import random
import base64
app = Flask(__name__, static_url_path='/static')

 

 

@app.route('/')

def home():

    return render_template('index.html')

 

@app.route('/submit_form', methods=['POST'])

def submit_form():

    # Get  azure form data

    subscription_id = request.form.get('subscription_id')

    client_id = request.form.get('client_id')

    client_secret = request.form.get('client_secret')

    tenant_id = request.form.get('tenant_id')
    User_name = request.form.get('User_name')
    User_Id = str(int(random.random()))
 

 

 

    # Write Azure form data to terraform.vars file

    with open('terraform.tfvars', 'w') as f:

        f.write(f'subscription_id = "{subscription_id}"\n')

        f.write(f'client_id = "{client_id}"\n')

        f.write(f'client_secret = "{client_secret}"\n')

        f.write(f'tenant_id = "{tenant_id}"\n')

    
    ## starting the script

    # Azure Resource Group and Key Vault Configuration
    resource_group_name = "prashant-rg"  
    key_vault_name = User_name + User_Id  
    secrets_file_path = "./terraform.tfvars"

    

    # Replace underscores with hyphens in the Key Vault and Resource Group names
    key_vault_name = key_vault_name.replace("_", "-")
    resource_group_name = resource_group_name.replace("_", "-")

    

    # Read secrets from the file
    secrets = {}
    with open(secrets_file_path, "r") as file:
        for line in file:
            key, value = line.strip().split(" = ")
            secrets[key] = value

    

    # Authenticate to Azure
    try:
        # Use Azure CLI to get the access token
        access_token = subprocess.check_output(["az", "account", "get-access-token", "--query", "accessToken", "-o", "tsv"]).decode("utf-8").strip()
    except subprocess.CalledProcessError:
        print("Error: Failed to obtain Azure access token. Make sure you are logged into Azure CLI.")
        exit(1)

    

    # # Create Azure Resource Group if it doesn't exist
    # try:
    #     subprocess.check_call(["az", "group", "create", "--name", resource_group_name, "--location", "southcentralus"])
    #     print(f"Azure Resource Group '{resource_group_name}' created successfully.")
    # except subprocess.CalledProcessError:
    #     print(f"Azure Resource Group '{resource_group_name}' already exists or encountered an error during creation.")

    

    # Create Azure Key Vault in the specified Resource Group
    try:
        subprocess.check_call(["az", "keyvault", "create", "--name", key_vault_name, "--resource-group", resource_group_name, "--location", "southcentralus"])
        print(f"Azure Key Vault '{key_vault_name}' created successfully in Resource Group '{resource_group_name}'.")
    except subprocess.CalledProcessError:
        print(f"Azure Key Vault '{key_vault_name}' already exists or encountered an error during creation in Resource Group '{resource_group_name}'.")

    

    # Store secrets in Azure Key Vault
    for key, value in secrets.items():
        # Replace underscores with hyphens in the secret name
        key = key.replace("_", "-")
        encoded_value = base64.b64encode(value.encode("utf-8")).decode("utf-8")     
        command = f"az keyvault secret set --vault-name {key_vault_name} --name {key} --value {encoded_value} --output none --query 'value'"
        # command = f"az keyvault secret set --vault-name {key_vault_name} --name {key} --value {value} --output none --query 'value'"

    

        try:
            # Use Azure CLI to set the secret in the Key Vault
            subprocess.check_call(["bash", "-c", f'AZURE_ACCESS_TOKEN="{access_token}" {command}'])
            print(f"Secret '{key}' stored in Azure Key Vault '{key_vault_name}' successfully.")
        except subprocess.CalledProcessError as e:
            print(f"Error: Failed to store secret '{key}' in Azure Key Vault '{key_vault_name}'.")
            print(e)

    

    print("All secrets have been stored in Azure Key Vault.")
    

    os.remove(secrets_file_path)     
    

    with open(secrets_file_path, "w"):         pass 
    

    ## ending the script

    return render_template('./file_submit.html')

 

if __name__ == '__main__':

    app.run(debug=True)